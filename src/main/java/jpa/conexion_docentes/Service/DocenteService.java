/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.conexion_docentes.Service;


import jpa.conexion_docentes.Repository.IDocenteRepo;
import jpa.conexion_docentes.modelo.vdocentes_ed;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

@Service
public class DocenteService {
    @Autowired
    private IDocenteRepo repo;
    
    public Page<vdocentes_ed> listardocentes(Pageable page){
        return repo.listar(page);
    }
    
    public List<vdocentes_ed> listardoc() {
        return repo.findAll();
    }
    
    public Page<vdocentes_ed> buscar(String codigo,Pageable page){ 
        return repo.filtrar(codigo, page);
    }
    
}
