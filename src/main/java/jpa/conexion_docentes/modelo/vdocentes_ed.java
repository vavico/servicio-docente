/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.conexion_docentes.modelo;

import java.io.Serializable;
import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "vdocentes_ed")
public class vdocentes_ed implements Serializable {

    @Id
    private String per_identificacion;
    @Column
    private String per_primerapellido;
    @Column
    private String per_primernombre;
    @Column
    private String per_segundonombre;
    @Column
    private String per_celular;
    @Column
    private String per_correo;
    @Column
    private Date per_fechanacimiento;

    public String getPer_identificacion() {
        return per_identificacion;
    }

    public void setPer_identificacion(String per_identificacion) {
        this.per_identificacion = per_identificacion;
    }

    public String getPer_primerapellido() {
        return per_primerapellido;
    }

    public void setPer_primerapellido(String per_primerapellido) {
        this.per_primerapellido = per_primerapellido;
    }

    public String getPer_primernombre() {
        return per_primernombre;
    }

    public void setPer_primernombre(String per_primernombre) {
        this.per_primernombre = per_primernombre;
    }

    public String getPer_segundonombre() {
        return per_segundonombre;
    }

    public void setPer_segundonombre(String per_segundonombre) {
        this.per_segundonombre = per_segundonombre;
    }

    public String getPer_celular() {
        return per_celular;
    }

    public void setPer_celular(String per_celular) {
        this.per_celular = per_celular;
    }

    public String getPer_correo() {
        return per_correo;
    }

    public void setPer_correo(String per_correo) {
        this.per_correo = per_correo;
    }

    public Date getPer_fechanacimiento() {
        return per_fechanacimiento;
    }

    public void setPer_fechanacimiento(Date per_fechanacimiento) {
        this.per_fechanacimiento = per_fechanacimiento;
    }

}
