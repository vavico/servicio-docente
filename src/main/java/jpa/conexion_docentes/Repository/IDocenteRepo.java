/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.conexion_docentes.Repository;

import jpa.conexion_docentes.modelo.vdocentes_ed;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;

@Repository
public interface IDocenteRepo extends JpaRepository<vdocentes_ed, Integer> {
	@Query(value = "select a from vdocentes_ed a order by a.per_identificacion")
    Page<vdocentes_ed> listar(Pageable page);
	
    
	@Query(value = "select a from vdocentes_ed a where a.per_identificacion like %:text% or a.per_primernombre like %:text% or a.per_segundonombre like %:text% or a.per_primerapellido like %:text% or a.per_celular like %:text% or a.per_correo like %:text%")
    Page<vdocentes_ed> filtrar(@Param("text") String text, Pageable page);
}
